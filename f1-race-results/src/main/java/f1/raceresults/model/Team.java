package f1.raceresults.model;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Holds information on a team.
 */
public class Team {

	/** The name of the team this record is for. */
	private String teamName;

	/** The number of points scored by the team this season. */
	private int points;

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this);
		builder.append("teamName", teamName).append("points", points);
		return builder.toString();
	}

}
