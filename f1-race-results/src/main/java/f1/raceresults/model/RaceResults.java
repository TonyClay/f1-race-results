package f1.raceresults.model;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Holds the race results data.
 */
public class RaceResults {

	/** List of drivers. */
	private List<Driver> drivers;

	/** List of teams. */
	private List<Team> teams;

	public List<Driver> getDrivers() {
		return drivers;
	}

	public void setDrivers(List<Driver> drivers) {
		this.drivers = drivers;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this);
		builder.append("drivers", drivers).append("teams", teams);
		return builder.toString();
	}

}
