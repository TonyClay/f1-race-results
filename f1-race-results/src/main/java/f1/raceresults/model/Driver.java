package f1.raceresults.model;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Holds information on a driver.
 */
public class Driver {

	/** The name of the driver this record is for. */
	private String driverName;

	/** The number of points scored by the driver this season. */
	private int points;

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this);
		builder.append("driverName", driverName).append("points", points);
		return builder.toString();
	}

}
