package f1.raceresults.filters;

import f1.raceresults.model.RaceResults;

/**
 * Filter a race results object.
 */
public interface F1RaceResultsFilter {

	/**
	 * Filter a race results object.
	 * 
	 * @param results
	 *            the results object to be filtered
	 */
	public void filter(RaceResults results);

}
