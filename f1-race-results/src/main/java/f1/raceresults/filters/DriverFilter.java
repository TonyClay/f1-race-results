package f1.raceresults.filters;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import f1.raceresults.model.Driver;
import f1.raceresults.model.RaceResults;

/**
 * Filter and sort the drivers list based on the number of points.
 */
public class DriverFilter implements F1RaceResultsFilter {

	/** The maximum number of drivers to include in the results. */
	private int maxNumOfDrivers;

	public int getMaxNumOfDrivers() {
		return maxNumOfDrivers;
	}

	public void setMaxNumOfDrivers(int maxNumOfDrivers) {
		this.maxNumOfDrivers = maxNumOfDrivers;
	}

	/**
	 * Filter the drivers list.
	 * 
	 * @param results
	 *            the results object to be filtered
	 */
	public void filter(RaceResults results) {
		List<Driver> drivers = results.getDrivers();
		Collections.sort(drivers, new Comparator<Driver>() {
			public int compare(Driver driver1, Driver driver2) {
				return driver2.getPoints() - driver1.getPoints();
			}
		});
		results.setDrivers(drivers.subList(0,
				Math.min(maxNumOfDrivers, drivers.size())));
	}

}
