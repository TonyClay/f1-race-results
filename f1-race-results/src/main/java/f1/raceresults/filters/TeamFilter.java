package f1.raceresults.filters;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import f1.raceresults.model.RaceResults;
import f1.raceresults.model.Team;

/**
 * Filter and sort the team list based on the number of points.
 */
public class TeamFilter implements F1RaceResultsFilter {

	/** The maximum number of teams to include in the results. */
	private int maxNumOfTeams;

	public int getMaxNumOfTeams() {
		return maxNumOfTeams;
	}

	public void setMaxNumOfTeams(int maxNumOfTeams) {
		this.maxNumOfTeams = maxNumOfTeams;
	}

	/**
	 * Filter the team list.
	 * 
	 * @param results
	 *            the results object to be filtered
	 */
	public void filter(RaceResults results) {
		List<Team> teams = results.getTeams();
		Collections.sort(teams, new Comparator<Team>() {
			public int compare(Team team1, Team team2) {
				return team2.getPoints() - team1.getPoints();
			}
		});
		results.setTeams(teams.subList(0, Math.min(maxNumOfTeams, teams.size())));
	}

}
