package f1.raceresults.processors;

import java.util.List;

import f1.raceresults.filters.F1RaceResultsFilter;
import f1.raceresults.model.RaceResults;
import f1.raceresults.services.F1RaceResultsService;
import f1.raceresults.transformers.F1RaceResultsJsonTransformer;

/**
 * Handles retrieving and formatting the race results.
 */
public class F1RaceResultsProcessor {

	/** Service to retrieve the race results data. */
	F1RaceResultsService service;

	/** Filters to sort and select the required records. */
	List<F1RaceResultsFilter> filters;

	/** Transformer to convert the results to JSON. */
	F1RaceResultsJsonTransformer transformer;

	public F1RaceResultsService getService() {
		return service;
	}

	public void setService(F1RaceResultsService service) {
		this.service = service;
	}

	public List<F1RaceResultsFilter> getFilters() {
		return filters;
	}

	public void setFilters(List<F1RaceResultsFilter> filters) {
		this.filters = filters;
	}

	public F1RaceResultsJsonTransformer getTransformer() {
		return transformer;
	}

	public void setTransformer(F1RaceResultsJsonTransformer transformer) {
		this.transformer = transformer;
	}

	/**
	 * Retrieve the required race results records in the correct order.
	 * 
	 * @return the race results
	 */
	public RaceResults getRaceResults() {
		RaceResults results = service.getRaceResults();
		for (F1RaceResultsFilter filter : filters) {
			filter.filter(results);
		}
		return results;
	}

	/**
	 * Retrieve the required race results in JSON format.
	 * 
	 * @return a String containing a JSON representation of the results
	 */
	public String getRaceResultsAsJson() {
		return transformer.toJson(getRaceResults());
	}

}
