package f1.raceresults.services;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import f1.raceresults.model.Driver;
import f1.raceresults.model.RaceResults;
import f1.raceresults.model.Team;

/**
 * Implementation of the service to fetch the race results from the HTML.
 */
public class F1RaceResultsServiceImpl implements F1RaceResultsService {

	/** Service to retrieve and parse the HTML. */
	private HtmlParser parser;

	public HtmlParser getParser() {
		return parser;
	}

	public void setParser(HtmlParser parser) {
		this.parser = parser;
	}

	/**
	 * Fetch the race results.
	 */
	public RaceResults getRaceResults() {
		return createResults(parser.getDocument());
	}

	/**
	 * Create a RaceResults object from a HTML Document object.
	 * 
	 * @param document
	 *            HTML Document object containing the data.
	 * @return the results
	 */
	private RaceResults createResults(Document document) {
		RaceResults results = new RaceResults();
		results.setDrivers(getDrivers(document));
		results.setTeams(getTeams(document));
		return results;
	}

	/**
	 * Extract the driver data from the HTML.
	 * 
	 * @param document
	 *            HTML Document object containing the data.
	 * @return the driver data
	 */
	private List<Driver> getDrivers(Document document) {
		Elements driverResults = document
				.select(".msr_season_driver_results tbody tr");
		List<Driver> drivers = new ArrayList<Driver>(driverResults.size());
		for (Element row : driverResults) {
			Driver driver = new Driver();
			driver.setDriverName(row.select(".msr_driver a").first().html());
			driver.setPoints(Integer.parseInt(row.select(".msr_total").first()
					.html()));
			drivers.add(driver);
		}
		return drivers;
	}

	/**
	 * Extract the team data from the HTML.
	 * 
	 * @param document
	 *            HTML Document object containing the data.
	 * @return the team data
	 */
	private List<Team> getTeams(Document document) {
		Elements teamResults = document
				.select(".msr_season_team_results tbody .msr_team");
		List<Team> teams = new ArrayList<Team>(teamResults.size());
		for (Element teamCell : teamResults) {
			Element row = teamCell.parent();
			Team team = new Team();
			team.setTeamName(teamCell.select("a").first().html());
			team.setPoints(Integer.parseInt(row.select(".msr_total").first()
					.html()));
			teams.add(team);
		}
		return teams;
	}

}
