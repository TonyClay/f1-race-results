package f1.raceresults.services;

import org.jsoup.nodes.Document;

/**
 * Convert a HTML document into an object model.
 * 
 */
public interface HtmlParser {

	/**
	 * Retrieve the HTML and convert in an object model.
	 * 
	 * @return the object model representing the document
	 */
	Document getDocument();

}
