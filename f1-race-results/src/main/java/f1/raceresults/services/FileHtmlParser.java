package f1.raceresults.services;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


/**
 * Convert a file containing an HTML document into an object model.
 */
public class FileHtmlParser implements HtmlParser {

	/** The file containing an HTML document to convert. */
	String htmlFile;

	public String getHtmlFile() {
		return htmlFile;
	}

	public void setHtmlFile(String htmlFile) {
		this.htmlFile = htmlFile;
	}

	/**
	 * Return the HTML document as an object model.
	 * 
	 * @return the object model representing the document
	 */
	public Document getDocument() {
		try {
			return Jsoup.parse(this.getClass().getResourceAsStream(htmlFile),
					null, "/");
		} catch (IOException excep) {
			throw new RuntimeException(excep);
		}
	}

}
