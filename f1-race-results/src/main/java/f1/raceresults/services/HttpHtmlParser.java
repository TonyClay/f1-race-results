package f1.raceresults.services;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Retrieve a HTML document from a URL and convert it to an object model.
 */
public class HttpHtmlParser implements HtmlParser {

	/** The url containing the HTML document to parse. */
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Return the HTML document as an object model.
	 * 
	 * @return the object model representing the document
	 */
	public Document getDocument() {
		try {
			return Jsoup.connect(url).get();
		} catch (IOException excep) {
			throw new RuntimeException("Error retrieving document", excep);
		}
	}

}
