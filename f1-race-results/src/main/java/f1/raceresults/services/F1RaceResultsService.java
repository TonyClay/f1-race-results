package f1.raceresults.services;

import f1.raceresults.model.RaceResults;

/**
 * Service to fetch the race results from the HTML.
 */
public interface F1RaceResultsService {

	/**
	 * Fetch the race results.
	 * 
	 * @return the results
	 */
	public RaceResults getRaceResults();

}
