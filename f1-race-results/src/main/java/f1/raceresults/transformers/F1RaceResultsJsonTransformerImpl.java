package f1.raceresults.transformers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import f1.raceresults.model.Driver;
import f1.raceresults.model.RaceResults;
import f1.raceresults.model.Team;

/**
 * Transform a RaceResults object into a JSON string.
 * 
 */
public class F1RaceResultsJsonTransformerImpl implements
		F1RaceResultsJsonTransformer {

	/**
	 * Transform a RaceResults object into a JSON string.
	 * 
	 * @param results
	 *            the results to convert
	 * @return string containing a JSON representation of the results
	 */
	public String toJson(RaceResults results) {
		JSONObject resultsJson = new JSONObject();
		resultsJson.put("drivers", convertDrivers(results));
		resultsJson.put("teams", convertTeams(results));
		return resultsJson.toString();
	}

	/**
	 * Convert the drivers data into a JSONArray object
	 * 
	 * @param results
	 *            the results to be converted
	 * @return the drivers data as JSONArray
	 */
	private JSONArray convertDrivers(RaceResults results) {
		JSONArray driversJson = new JSONArray();
		for (Driver driver : results.getDrivers()) {
			JSONObject driverJson = new JSONObject();
			driverJson.put("driverName", driver.getDriverName());
			driverJson.put("points", driver.getPoints());
			driversJson.add(driverJson);
		}
		return driversJson;
	}

	/**
	 * Convert the teams data into a JSONArray object
	 * 
	 * @param results
	 *            the results to be converted
	 * @return the teams data as JSONArray
	 */
	private JSONArray convertTeams(RaceResults results) {
		JSONArray teamsJson = new JSONArray();
		for (Team team : results.getTeams()) {
			JSONObject teamJson = new JSONObject();
			teamJson.put("teamName", team.getTeamName());
			teamJson.put("points", team.getPoints());
			teamsJson.add(teamJson);
		}
		return teamsJson;
	}

}
