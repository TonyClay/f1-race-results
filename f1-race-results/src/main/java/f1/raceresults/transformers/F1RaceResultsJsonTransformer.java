package f1.raceresults.transformers;

import f1.raceresults.model.RaceResults;

/**
 * Transform a RaceResults object into a JSON string.
 */
public interface F1RaceResultsJsonTransformer {

	/**
	 * Transform a RaceResults object into a JSON string.
	 * 
	 * @param results
	 *            the results to convert
	 * @return string containing a JSON representation of the results
	 */
	public String toJson(RaceResults results);
}
