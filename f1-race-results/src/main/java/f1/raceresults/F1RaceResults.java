package f1.raceresults;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import f1.raceresults.processors.F1RaceResultsProcessor;

/**
 * Get the race results and output them as JSON to the console.
 */
public class F1RaceResults {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"f1/raceresults/application-context.xml");
		F1RaceResultsProcessor processor = (F1RaceResultsProcessor) ctx
				.getBean("processor");
		System.out.println(processor.getRaceResultsAsJson());
	}
}
