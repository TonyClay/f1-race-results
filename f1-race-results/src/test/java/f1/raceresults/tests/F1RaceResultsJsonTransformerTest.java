package f1.raceresults.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import f1.raceresults.model.Driver;
import f1.raceresults.model.RaceResults;
import f1.raceresults.model.Team;
import f1.raceresults.transformers.F1RaceResultsJsonTransformerImpl;

public class F1RaceResultsJsonTransformerTest {

	@Test
	public void testPopulated() {
		RaceResults results = new RaceResults();
		results.setDrivers(Arrays.asList(new Driver[] {
				TestUtils.createDriver("Driver 1", 1),
				TestUtils.createDriver("Driver 2", 2),
				TestUtils.createDriver("Driver 3", 3),
				TestUtils.createDriver("Driver 4", 4),
				TestUtils.createDriver("Driver 5", 5) }));
		results.setTeams(Arrays.asList(new Team[] {
				TestUtils.createTeam("Team 1", 1),
				TestUtils.createTeam("Team 2", 2),
				TestUtils.createTeam("Team 3", 3) }));
		F1RaceResultsJsonTransformerImpl trans = new F1RaceResultsJsonTransformerImpl();
		assertEquals("{\"drivers\":["
				+ "{\"points\":1,\"driverName\":\"Driver 1\"},"
				+ "{\"points\":2,\"driverName\":\"Driver 2\"},"
				+ "{\"points\":3,\"driverName\":\"Driver 3\"},"
				+ "{\"points\":4,\"driverName\":\"Driver 4\"},"
				+ "{\"points\":5,\"driverName\":\"Driver 5\"}"
				+ "],\"teams\":[" + "{\"teamName\":\"Team 1\",\"points\":1},"
				+ "{\"teamName\":\"Team 2\",\"points\":2},"
				+ "{\"teamName\":\"Team 3\",\"points\":3}" + "]}",
				trans.toJson(results));
	}

	@Test
	public void testEmptyDrivers() {
		RaceResults results = new RaceResults();
		results.setDrivers(Arrays.asList(new Driver[] {}));
		results.setTeams(Arrays.asList(new Team[] {
				TestUtils.createTeam("Team 1", 1),
				TestUtils.createTeam("Team 2", 2),
				TestUtils.createTeam("Team 3", 3) }));
		F1RaceResultsJsonTransformerImpl trans = new F1RaceResultsJsonTransformerImpl();
		assertEquals("{\"drivers\":[],\"teams\":["
				+ "{\"teamName\":\"Team 1\",\"points\":1},"
				+ "{\"teamName\":\"Team 2\",\"points\":2},"
				+ "{\"teamName\":\"Team 3\",\"points\":3}" + "]}",
				trans.toJson(results));
	}

	@Test
	public void testEmptyTeams() {
		RaceResults results = new RaceResults();
		results.setDrivers(Arrays.asList(new Driver[] {
				TestUtils.createDriver("Driver 1", 1),
				TestUtils.createDriver("Driver 2", 2),
				TestUtils.createDriver("Driver 3", 3),
				TestUtils.createDriver("Driver 4", 4),
				TestUtils.createDriver("Driver 5", 5) }));
		results.setTeams(Arrays.asList(new Team[] {}));
		F1RaceResultsJsonTransformerImpl trans = new F1RaceResultsJsonTransformerImpl();
		assertEquals("{\"drivers\":["
				+ "{\"points\":1,\"driverName\":\"Driver 1\"},"
				+ "{\"points\":2,\"driverName\":\"Driver 2\"},"
				+ "{\"points\":3,\"driverName\":\"Driver 3\"},"
				+ "{\"points\":4,\"driverName\":\"Driver 4\"},"
				+ "{\"points\":5,\"driverName\":\"Driver 5\"}"
				+ "],\"teams\":[]}", trans.toJson(results));
	}

}
