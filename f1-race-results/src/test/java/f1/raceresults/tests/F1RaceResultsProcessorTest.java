package f1.raceresults.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import f1.raceresults.filters.DriverFilter;
import f1.raceresults.filters.F1RaceResultsFilter;
import f1.raceresults.filters.TeamFilter;
import f1.raceresults.model.Driver;
import f1.raceresults.model.RaceResults;
import f1.raceresults.model.Team;
import f1.raceresults.processors.F1RaceResultsProcessor;
import f1.raceresults.services.F1RaceResultsService;

public class F1RaceResultsProcessorTest {

	public F1RaceResultsProcessor processor;

	@Before
	public void setupProcessor() {
		processor = new F1RaceResultsProcessor();
		F1RaceResultsService service = EasyMock
				.createMock(F1RaceResultsService.class);
		processor.setService(service);
		RaceResults results = new RaceResults();
		results.setDrivers(Arrays.asList(new Driver[] {
				TestUtils.createDriver("Driver 1", 1),
				TestUtils.createDriver("Driver 2", 2),
				TestUtils.createDriver("Driver 3", 3),
				TestUtils.createDriver("Driver 4", 4),
				TestUtils.createDriver("Driver 5", 5),
				TestUtils.createDriver("Driver 6", 6),
				TestUtils.createDriver("Driver 7", 7) }));
		results.setTeams(Arrays.asList(new Team[] {
				TestUtils.createTeam("Team 1", 1),
				TestUtils.createTeam("Team 2", 2),
				TestUtils.createTeam("Team 3", 3),
				TestUtils.createTeam("Team 4", 4),
				TestUtils.createTeam("Team 5", 5) }));
		EasyMock.expect(service.getRaceResults()).andReturn(results).once();
		EasyMock.replay(service);
		processor.setFilters(new ArrayList<F1RaceResultsFilter>());
		DriverFilter driverFilter = new DriverFilter();
		driverFilter.setMaxNumOfDrivers(5);
		processor.getFilters().add(driverFilter);
		TeamFilter teamFilter = new TeamFilter();
		teamFilter.setMaxNumOfTeams(3);
		processor.getFilters().add(teamFilter);
	}

	@Test
	public void test() {
		RaceResults results = processor.getRaceResults();
		assertEquals(5, results.getDrivers().size());
		assertEquals("Driver 7", results.getDrivers().get(0).getDriverName());
		assertEquals(3, results.getTeams().size());
		assertEquals("Team 5", results.getTeams().get(0).getTeamName());
	}

}
