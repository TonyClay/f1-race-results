package f1.raceresults.tests;

import f1.raceresults.model.Driver;
import f1.raceresults.model.Team;

/**
 * Utility functions to help with unit tests.
 * 
 */
public class TestUtils {

	/**
	 * Create a Driver object.
	 * 
	 * @param driverName
	 *            the driver name
	 * @param points
	 *            the number of points
	 * @return a Driver object
	 */
	public static Driver createDriver(String driverName, int points) {
		Driver driver = new Driver();
		driver.setDriverName(driverName);
		driver.setPoints(points);
		return driver;
	}

	/**
	 * Create a Team object.
	 * 
	 * @param teamName
	 *            the team name
	 * @param points
	 *            the number of points
	 * @return a Team object
	 */
	public static Team createTeam(String teamName, int points) {
		Team team = new Team();
		team.setTeamName(teamName);
		team.setPoints(points);
		return team;
	}

}
