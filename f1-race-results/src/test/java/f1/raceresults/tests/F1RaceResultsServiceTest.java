package f1.raceresults.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import f1.raceresults.model.RaceResults;
import f1.raceresults.services.F1RaceResultsServiceImpl;
import f1.raceresults.services.FileHtmlParser;

/**
 * Unit test for F1RaceResults.
 */
public class F1RaceResultsServiceTest {

	/**
	 * Test that the service produces results from HTML file.
	 */
	@Test
	public void testGetRaceResults() {
		F1RaceResultsServiceImpl service = new F1RaceResultsServiceImpl();
		FileHtmlParser parser = new FileHtmlParser();
		parser.setHtmlFile("/f1/raceresults/test-input.html");
		service.setParser(parser);
		RaceResults raceResults = service.getRaceResults();
		assertEquals(raceResults.getDrivers().size(), 4);
		assertEquals(raceResults.getDrivers().get(0).getDriverName(),
				"Driver 1");
		assertEquals(raceResults.getDrivers().get(0).getPoints(), 99);
		assertEquals(raceResults.getTeams().size(), 3);
		assertEquals(raceResults.getTeams().get(0).getTeamName(), "Team 1");
		assertEquals(raceResults.getTeams().get(0).getPoints(), 99);
	}

}
