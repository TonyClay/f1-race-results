package f1.raceresults.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import f1.raceresults.filters.TeamFilter;
import f1.raceresults.model.RaceResults;
import f1.raceresults.model.Team;

public class TeamFilterTest {

	private final TeamFilter filter = new TeamFilter();
	private RaceResults results;

	@Before
	public void setupResults() {
		results = new RaceResults();
		List<Team> teams = new ArrayList<Team>();
		teams.add(TestUtils.createTeam("Team 1", 4));
		teams.add(TestUtils.createTeam("Team 2", 1));
		teams.add(TestUtils.createTeam("Team 3", 5));
		teams.add(TestUtils.createTeam("Team 4", 3));
		teams.add(TestUtils.createTeam("Team 5", 2));
		results.setTeams(teams);
	}

	@Test
	public void testFilterAndSort() {
		filter.setMaxNumOfTeams(3);
		filter.filter(results);
		assertEquals(3, results.getTeams().size());
		assertEquals("Team 3", results.getTeams().get(0).getTeamName());
		assertEquals("Team 1", results.getTeams().get(1).getTeamName());
		assertEquals("Team 4", results.getTeams().get(2).getTeamName());
	}

	@Test
	public void testSortOnly() {
		filter.setMaxNumOfTeams(6);
		filter.filter(results);
		assertEquals(5, results.getTeams().size());
		assertEquals("Team 3", results.getTeams().get(0).getTeamName());
		assertEquals("Team 1", results.getTeams().get(1).getTeamName());
		assertEquals("Team 4", results.getTeams().get(2).getTeamName());
		assertEquals("Team 5", results.getTeams().get(3).getTeamName());
		assertEquals("Team 2", results.getTeams().get(4).getTeamName());
	}

}
