package f1.raceresults.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import f1.raceresults.filters.DriverFilter;
import f1.raceresults.model.Driver;
import f1.raceresults.model.RaceResults;

public class DriverFilterTest {

	private final DriverFilter filter = new DriverFilter();
	private RaceResults results;

	@Before
	public void setupResults() {
		results = new RaceResults();
		List<Driver> drivers = new ArrayList<Driver>();
		drivers.add(TestUtils.createDriver("Driver 1", 5));
		drivers.add(TestUtils.createDriver("Driver 2", 3));
		drivers.add(TestUtils.createDriver("Driver 3", 2));
		drivers.add(TestUtils.createDriver("Driver 4", 1));
		drivers.add(TestUtils.createDriver("Driver 5", 4));
		results.setDrivers(drivers);
	}

	@Test
	public void testFilterAndSort() {
		filter.setMaxNumOfDrivers(3);
		filter.filter(results);
		assertEquals(3, results.getDrivers().size());
		assertEquals("Driver 1", results.getDrivers().get(0).getDriverName());
		assertEquals("Driver 5", results.getDrivers().get(1).getDriverName());
		assertEquals("Driver 2", results.getDrivers().get(2).getDriverName());
	}

	@Test
	public void testSortOnly() {
		filter.setMaxNumOfDrivers(6);
		filter.filter(results);
		assertEquals(5, results.getDrivers().size());
		assertEquals("Driver 1", results.getDrivers().get(0).getDriverName());
		assertEquals("Driver 5", results.getDrivers().get(1).getDriverName());
		assertEquals("Driver 2", results.getDrivers().get(2).getDriverName());
		assertEquals("Driver 3", results.getDrivers().get(3).getDriverName());
		assertEquals("Driver 4", results.getDrivers().get(4).getDriverName());
	}

}
