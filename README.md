# README #

Command line application to produce F1 Race Results information written for a technical assessment for JD Williams.

## Building and running the application ##

Requires maven to build the application. From the command line, cd into the f1-race-results directory and type the following:

	mvn package

Once built the application can be run by typing:

	java -jar target/f1-race-results-0.0.1-SNAPSHOT-jar-with-dependencies.jar

## Bugs ##

While testing there was a problem with an ad in the live page which caused the parser to fail to read the HTML correctly. The application is currently running with a saved copy of the live page with the issue in the ad code fixed. To revert to running with the live page, see the comment in the following file:

	f1-race-results/src/main/resources/f1/raceresults/application-context.xml